let data = getChartData([], '', [], [], []);
const configTest = {
  type: 'line',
  data: data,
  options: {maintainAspectRatio: false}
};
var chartTest = new Chart(
    document.getElementById('chartTest'),
    configTest
  );

function getChartData(labels, 
                      label_title,
                      cases,
                      model0,
                      model1) {
    const data = {
      labels: labels,
      datasets: [{
        label: label_title,
        backgroundColor: 'rgb(255, 99, 132)',
        borderColor: 'rgb(255, 99, 132)',
        data: cases
      }, {
        label: 'scenario 0',
        backgroundColor: 'rgb(99, 132, 255)',
        borderColor: 'rgb(99, 132, 255)',
        data: model0
      }, {
        label: 'scenario 1',
        backgroundColor: 'rgb(99, 255, 132)',
        borderColor: 'rgb(99, 255, 132)',
        data: model1
      }]
    };
    
    return data;
}

class ChartSelector extends React.Component {
  constructor(props) {
    super(props);
    this.state = { chart: "Fall"};
    this.onChangeValue = this.onChangeValue.bind(this);
  }
  
  onChangeValue(event) {
    this.setState({chart: event.target.value});
  }


  render() {
    const selectedChart = this.state.chart;
    let data;
    if (selectedChart === "Fall") {
      data = getChartData(
        labels,
        'antal fall/dag',
        data_cases,
        data_model0,
        data_model1);
    } else if (selectedChart === "IVA") {
      data = getChartData(
        labels_iva,
        'antal IVA fall/dag',
        data_cases_iva,
        data_model0_iva,
        data_model1_iva
      );
    } else if (selectedChart === "Slutenvård") {
      data = getChartData(
        labels_slutenv,
        'antal slutenvårdade/dag',
        data_cases_slutenv,
        data_model0_slutenv,
        data_model1_slutenv
      );
    };
    chartTest.data = data;
    chartTest.update();
    return (
      <div>
      <div className="btn-group" role="group">
        <input type="radio" className="btn-check" name="btnradio" value="Fall" id="btnfall" autoComplete="off" defaultChecked onChange={this.onChangeValue}/>
        <label className="btn btn-outline-primary" htmlFor="btnfall">Fall</label>
          
        <input type="radio" className="btn-check" name="btnradio" value="IVA" id="btniva" autoComplete="off" onChange={this.onChangeValue} />
        <label className="btn btn-outline-primary" htmlFor="btniva">IVA</label>
          
        <input type="radio" className="btn-check" name="btnradio" value="Slutenvård" id="btnslutenv" autoComplete="off" onChange={this.onChangeValue} />
        <label className="btn btn-outline-primary" htmlFor="btnslutenv">Slutenvård</label>
      </div>
      <h4 className="text-center">{this.state.chart}</h4>
      </div>
    )
  }
}

ReactDOM.render(
    <div>
    <h3>FHM prognosmodeller</h3>
    <p>Hur väl matchar Folkhälsomyndighetens modeller mot verkligheten. Nedan ses interimsrapport jan 2022.</p>
    < ChartSelector />
    </div>,
    document.getElementById('root')
);