import sys
import os
import json
import urllib
from datetime import datetime, date, timedelta
sys.path.append(
    os.path.join(
        os.path.dirname(__file__),
        '../.venv/lib/python3.8/site-packages'))

# External packages
from flask import Flask, render_template
from bs4 import BeautifulSoup
import yaml
import pandas as pd

app = Flask(__name__)

def get_date(microsoft_json_date):
    date = datetime(1970, 1, 1) + timedelta(milliseconds=microsoft_json_date)
    return date.strftime("%Y-%m-%d")

@app.route("/")
@app.route("/home")
def hello_world():
    return render_template("home.html")


@app.route("/wellstep")
def wellstep():
    with open("../data/wellstep/Wellstep.html", 'r') as file:
        index = file.read()
        soup = BeautifulSoup(index, 'html.parser')

    html_table = soup.find_all("table", {"class":'table table-hover'})
    score_df = pd.read_html(str(html_table))[0]

    # Recalculate scores
    def get_late_starters(file):
        with open(file, 'r') as fp:
            late_starters = yaml.full_load(fp)
        return late_starters

    def adjust_score(original_score, late_starters,
                     today=date(2022, 4, 10),
                     start_date=date(2022, 1, 31)):
        adjusted_score = original_score.copy()
        adjusted_score["Poäng/dag"] = adjusted_score.apply(adjust_averages,
                                                           args=(today, late_starters),
                                                           axis=1)
        adjusted_score["Poäng"] = adjusted_score.apply(recalc_tot_score,
                                                     args=(today, start_date, late_starters),
                                                     axis=1)
        return adjusted_score

    def adjust_averages(score_row, today, late_starters):
        if score_row['Namn'] in late_starters:
            active_time = today - late_starters[score_row['Namn']] + timedelta(days=1)  # add one day since we include both start and end date
            clean_points = float(score_row['Poäng'].replace(" ", ""))
            avg_points = clean_points / active_time.days
            return float(avg_points)
        else:
            return float(score_row['Poäng/dag'])

    def recalc_tot_score(score_row, today, start_date, late_starters):
        if score_row['Namn'] in late_starters:
            all_days = (today - start_date + timedelta(days=1)).days
            return int(score_row["Poäng/dag"]*all_days)
        else:
            return int(score_row['Poäng'].replace(" ", ""))

    original_score=score_df
    late_starter_file = "../data/wellstep/late_starters.yaml" 
    late_starters = get_late_starters(late_starter_file)
    adjusted_score=adjust_score(original_score, late_starters)
    adjusted_score = adjusted_score.sort_values(by=['Poäng'], ascending=False)
    adjusted_score = adjusted_score.round(1)
    team_scores = adjusted_score.groupby(['Lag']).mean()
    team_scores = team_scores.round({'Poäng': 1})
    team_scores = team_scores.sort_values(by=['Poäng'], ascending=False)
    total_score = adjusted_score['Poäng'].sum()
    return render_template("wellstep.html", scores=adjusted_score,
                           team_scores=team_scores, total=total_score)

@app.route("/models")
def models():

    def read_model(modelfile, sheet):
        model = pd.read_excel(modelfile, sheet_name=sheet)
        return model
    modelfile="../data/model7b_per_day.xlsx"
    model = read_model(modelfile, sheet="Riket_Scenario0")
    model1 = read_model(modelfile, sheet="Riket_Scenario1")
    model1.rename(columns={'Sim_antal_fall': 'fall_m1',
                           'Sim_iva': 'iva_m1',
                           'Sim_sjukhusvård': 'slutenv_m1'}, inplace=True)

    url = "https://services5.arcgis.com/fsYDFeRKu1hELJJs/arcgis/rest/services/FOHM_Covid_19_FME_1/FeatureServer/1/query?f=geojson&where=1%3D1&outFields=*&orderByFields=Statistikdatum%20desc"
    geojson_str = urllib.request.urlopen(url).read()
    geojson = json.loads(geojson_str)
    df = pd.json_normalize(geojson["features"])
    df['properties.Statistikdatum'] = df['properties.Statistikdatum'].apply(get_date) 

    df.rename(columns={'properties.Statistikdatum': 'Datum', 
                       'properties.Totalt_antal_fall': 'Fall',
                       'properties.Antal_intensivvardade': 'IVA'}, inplace=True)

    cases_df = pd.merge(model[['Datum', 'Sim_antal_fall']], 
                             df[['Datum', 'Fall']], on=['Datum', 'Datum'],
                             how='left')
    cases_df = pd.merge(model1[['Datum', 'fall_m1']], 
                             cases_df, on=['Datum', 'Datum'],
                             how='left')
    dates = list(cases_df['Datum'])
    cases = list(cases_df['Fall'])
    model_0 = list(cases_df['Sim_antal_fall'])
    model_1 = list(cases_df['fall_m1'])

    iva_df = pd.merge(model[['Datum', 'Sim_iva']],
                      df[['Datum', 'IVA']], on=['Datum', 'Datum'],
                      how='left')
    iva_df = pd.merge(model1[['Datum', 'iva_m1']],
                      iva_df, on=['Datum', 'Datum'],
                      how='left')
    dates_iva = list(iva_df['Datum'])
    cases_iva = list(iva_df['IVA'])
    model_0_iva = list(iva_df['Sim_iva'])
    model_1_iva = list(iva_df['iva_m1'])

    # 'Sim_sjukhusvård'
    url = "https://www.socialstyrelsen.se/globalassets/sharepoint-dokument/dokument-webb/statistik/Utv_tid_rullande.csv"
    df_slutenvard = pd.read_csv(url, sep=",")
    df_slutenvard.rename(columns={'datum': 'Datum'}, inplace=True)
    slutenv_df = pd.merge(model[['Datum', 'Sim_sjukhusvård']],
                          df_slutenvard[['Datum', 'Inskrivna i slutenvård - antal']],
                          on= ['Datum', 'Datum'], how='left')
    slutenv_df = pd.merge(model1[['Datum', 'slutenv_m1']],
                          slutenv_df,
                          on= ['Datum', 'Datum'], how='left')
    dates_slutenv = list(slutenv_df['Datum'])
    cases_slutenv = list(slutenv_df['Inskrivna i slutenvård - antal'])
    model_0_slutenv = list(slutenv_df['Sim_sjukhusvård'])
    model_1_slutenv = list(slutenv_df['slutenv_m1'])

    return render_template("models.html", dates=json.dumps(dates),
                           cases=json.dumps(cases),
                           model_0=json.dumps(model_0),
                           model_1=json.dumps(model_1),
                           dates_iva=json.dumps(dates_iva),
                           cases_iva=json.dumps(cases_iva),
                           model_0_iva=json.dumps(model_0_iva),
                           model_1_iva=json.dumps(model_1_iva),
                           dates_slutenv=json.dumps(dates_slutenv),
                           cases_slutenv=json.dumps(cases_slutenv),
                           model_0_slutenv=json.dumps(model_0_slutenv),
                           model_1_slutenv=json.dumps(model_1_slutenv))    

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8888, debug=True) 
