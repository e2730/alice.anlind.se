# alice.anlind.se
This repo contains the code for my local webbpage that I use to test things out and analyse things on the fly.

# Local testing 
To test this locally simply run:  
```
python public_html/src/app.py
```

# Deployment
This webbpage is deployed to Lopia webbhotel via FTP and using CGI to host. To include libraries a virtual enviroment is used. The installed packages are transfered to the hotel instead of being installed.

## Modell evaluering FHM
Iden är att validera om FHMs modeller varit bra eller dåliga.
Första modellen att testa borde vara den som gäller nu för att få
ett hum om vart än det bär, men med tiden vore det härligt
att även lägga in äldre versioner.

Datakällor
Covid fall per dag och region
(csv fungerar ej)
(html ser lovande ut)

https://www.dataportal.se/sv/datasets/525_1424/antal-fall-av-covid-19-i-sverige-per-dag-och-region#ref=?p=1&q=covid&s=2&t=20&f=&rt=dataset%24esterms_IndependentDataService%24esterms_ServedByDataService&c=false 

Covid sjukvård per dag och region
https://www.folkhalsomyndigheten.se/globalassets/statistik-uppfoljning/smittsamma-sjukdomar/veckorapporter-covid-19/2020/overvakningssystem-for-covid-19-v4.pdf

https://www.socialstyrelsen.se/statistik-och-data/statistik/statistik-om-covid-19/


Covid IVA per dag och region

## Energimyndigheten, Svenska Kraftnät,