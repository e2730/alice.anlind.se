#!/bin/bash

# Activate the enviroment to get correct python packages
source /home/pi/alice.anlind.se/.venv/bin/activate

# Export the path to flask app to the Python Path
export PYTHONPATH=${PYTHONPATH}:/home/pi/alice.anlind.se/public_html/src

# Start serving the app
twistd --nodaemon --pidfile= web --wsgi app.app --listen tcp:80 --logfile log.txt
